@extends('layouts.app')

@section('title', '5 Minute Cleanup')

@section('content')
    <div class="intro">
        <img class="img-fluid" src="{{ asset('images/cleanup.svg') }}">
    </div>
    <div class="collaboration py-5">
        In collaboration with
        <div class="container">
          <div class="row my-4">
            <div class="col-sm d-flex align-items-center justify-content-around">
              <img width="150" src="{{ asset('images/logos/mta.svg') }}" alt="Malta Tourism Authority">
            </div>
            <div class="col-sm d-flex align-items-center justify-content-around">
              <img width="150" src="{{ asset('images/logos/which-beach.svg') }}" alt="Which Beach">
            </div>
            <div class="col-sm d-flex align-items-center justify-content-around">
              <img width="150" src="{{ asset('images/logos/zibel.svg') }}" alt="Zibel">
            </div>
            <div class="col-sm d-flex align-items-center justify-content-around">
              <img width="150" src="{{ asset('images/logos/xfm.png') }}" alt="Xfm">
            </div>
          </div>
          <div class="row">
            <div class="col">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/WoEJvD7TxRw?rel=0"></iframe>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="steps py-5">
        <div class="container">
          <div class="row">
            <div class="col">
                <h3>3 Easy Steps</h3>
            </div>
          </div>
          <div class="row my-5">
            <div class="col-md">
                <img class="mb-3" src="{{ asset('images/steps/search.svg') }}">
                <h2>Search</h2>
                <p>Look in the immediate area around you</p>
            </div>
            <div class="col-md">
                <img class="mb-3" src="{{ asset('images/steps/collect.svg') }}">
              <h2>Collect</h2>
              <p>Spend 5 minutes collecting everything you can find</p>
            </div>
            <div class="col-md">
                <img class="mb-3" src="{{ asset('images/steps/dispose.svg') }}">
              <h2>Dispose</h2>
              <p>Dispose of them in a nearby bin</p>
            </div>
          </div>
          <div class="row">
            <div class="col">
                <h3>Make a difference. Be the change.</h3>
            </div>
          </div>
        </div>
    </div>
@endsection
